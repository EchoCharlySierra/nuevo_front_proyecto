# \<gold-iban-input\>

[![Build status](https://travis-ci.org/ttretau/gold-iban-input.svg?branch=master)](https://travis-ci.org/ttretau/gold-iban-input)

Material Design <a href="https://en.wikipedia.org/wiki/International_Bank_Account_Number">IBAN</a> Input Element:
Single-line text field with Material Design styling
for entering a IBAN. As the user types, the number will be formatted by adding a space every 4 digits and remaining digits.

```html
<gold-iban-input label="IBAN"></gold-iban-input>
```

## Validation
The input is validated via country based length and checksum.
